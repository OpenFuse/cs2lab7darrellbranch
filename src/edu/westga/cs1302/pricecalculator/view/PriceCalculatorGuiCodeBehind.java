package edu.westga.cs1302.pricecalculator.view;

import edu.westga.cs1302.pricecalculator.viewmodel.PriceCalculatorViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * The Class PriceCalculatorGUICodeBehind.
 * 
 * @author Darrell Branch
 */
public class PriceCalculatorGuiCodeBehind {
	
	@FXML
	private TextField unitPriceField;
	
	@FXML
	private TextField quantityField;
	
	@FXML
	private TextField maxDiscountField;
	
	@FXML
	private Button generateButton;
	
	@FXML
	private TextArea priceAnalysisField;
	
	private PriceCalculatorViewModel viewModel;
	
	/**
	 * Instantiates a new price calculator GUI code behind.
	 */
	public PriceCalculatorGuiCodeBehind() {
		this.viewModel = new PriceCalculatorViewModel();
	}
	
	
	@FXML
	private void initialize() {
		this.priceAnalysisField.setEditable(false);
		
		this.unitPriceField.textProperty().bindBidirectional(this.viewModel.getUnitPriceProperty());
		this.quantityField.textProperty().bindBidirectional(this.viewModel.getQuantityProperty());
		this.maxDiscountField.textProperty().bindBidirectional(this.viewModel.getMaxDiscountProperty());
		this.priceAnalysisField.textProperty().bind(this.viewModel.getPriceAnalysisProperty());
	}
	
	@FXML
    void handleSubmit() {
		this.viewModel.generatePriceAnalysis();
    }

}
