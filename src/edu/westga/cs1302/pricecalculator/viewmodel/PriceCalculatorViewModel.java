package edu.westga.cs1302.pricecalculator.viewmodel;

import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.pricecalculator.model.PriceCalculator;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The Class PriceCalculatorViewModel.
 * 
 * @author Darrell Branch
 */
public class PriceCalculatorViewModel {
	
	private StringProperty unitPriceProperty;
	
	private StringProperty quantityProperty;
	
	private StringProperty maxDiscountProperty;
	
	private StringProperty priceAnalysisProperty;
	
	private PriceCalculator priceCalculator;
	
	/**
	 * Instantiates a new price calculator view model.
	 */
	public PriceCalculatorViewModel() {
		this.unitPriceProperty = new SimpleStringProperty();
		this.quantityProperty = new SimpleStringProperty();
		this.maxDiscountProperty = new SimpleStringProperty();
		this.priceAnalysisProperty = new SimpleStringProperty();
		this.priceCalculator = new PriceCalculator();
	}

	/**
	 * Gets the unit price property.
	 *
	 * @return the unit price property
	 */
	public StringProperty getUnitPriceProperty() {
		return this.unitPriceProperty;
	}

	/**
	 * Gets the quantity property.
	 *
	 * @return the quantity property
	 */
	public StringProperty getQuantityProperty() {
		return this.quantityProperty;
	}

	/**
	 * Gets the max discount property.
	 *
	 * @return the max discount property
	 */
	public StringProperty getMaxDiscountProperty() {
		return this.maxDiscountProperty;
	}

	/**
	 * Gets the price analysis property.
	 *
	 * @return the price analysis property
	 */
	public StringProperty getPriceAnalysisProperty() {
		return this.priceAnalysisProperty;
	}
	
	/**
	 * Generate price analysis.
	 */
	public void generatePriceAnalysis() {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);
		Double unitPrice = Double.parseDouble(this.unitPriceProperty.get());
		Integer quantity = Integer.parseInt(this.quantityProperty.get());
		Integer maxDiscount = Integer.parseInt(this.maxDiscountProperty.get());
		
		this.priceCalculator.setUnitPrice(unitPrice);
		this.priceCalculator.setQuantity(quantity);

		String output = "";
		output += String.format("%-30s %-30s %-30s\n", "Unit Price", "Quantity", "Total Payment");
		output += String.format("%-30s %-30s %-30s\n", currencyFormatter.format(unitPrice), quantity.toString(), currencyFormatter.format(this.priceCalculator.calculateRegularTotal()));
		output += System.lineSeparator();
		output += String.format("%-30s %-30s %-30s\n", "Discount %", "Price w/discount", "Savings");
		
		int i = 0;
		while (i <= maxDiscount) {
			Double newPrice = this.priceCalculator.calculateDiscountedTotal(i);
			Double savings = this.priceCalculator.calculateTotalSavings(i);
			output += String.format("%-30s %-30s %-30s\n", i + "%", currencyFormatter.format(newPrice), currencyFormatter.format(savings));		
			i += 5;
		}
		
		this.priceAnalysisProperty.set(output);
	}
	
	
}
