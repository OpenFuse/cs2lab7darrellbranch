package edu.westga.cs1302.pricecalculator.model;

/**
 * The Class PriceCalculator.
 * 
 * @author CS1302
 * @version Lab 7 - Fall 2018
 */
public class PriceCalculator {
	private static final String DISCOUNT_PERCENTAGE_OUT_OF_RANGE = "discountPercentage must be >= 0 and <= 100.";
	private static final String UNIT_PRICE_MUST_BE_GREATER_THAN_0 = "unitPrice must be >= 0";
	private static final String QUANTITY_MUST_BE_GREATER_THAN_OR_EQUAL_TO_0 = "quantity must be >= 0";

	private double unitPrice;
	private int quantity;

	/**
	 * Instantiates a new price calculator.
	 *
	 * @precondition none
	 * @postcondition getUnitPrice() == 0 AND getQuantity() == 0
	 * 
	 */
	public PriceCalculator() {
		this.unitPrice = 0;
		this.quantity = 0;
	}

	/**
	 * Calculates the discounted UNIT price based on the discountPercentage.
	 * Discount percentage is a value between 0 and 100.
	 *
	 * @precondition discountPercentage >= 0 and <= 100
	 * @postcondition none
	 * 
	 * @param discountPercentage the discount percentage
	 * 
	 * @return the amount of the monthly payment
	 */
	public double calculateDiscountedUnitPrice(double discountPercentage) {
		if (discountPercentage < 0 || discountPercentage > 100) {
			throw new IllegalArgumentException(DISCOUNT_PERCENTAGE_OUT_OF_RANGE);
		}

		return this.unitPrice * (1 - discountPercentage / 100);
	}

	/**
	 * Calculate regular total.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the double
	 */
	public double calculateRegularTotal() {
		return this.quantity * this.unitPrice;
	}

	/**
	 * Calculate discounted total.
	 * 
	 * @precondition discountPercetage >= 0 and <= 100
	 * @postcondition none
	 *
	 * @param discountPercentage the discount percentage
	 * 
	 * @return the total price after applying the discount
	 */
	public double calculateDiscountedTotal(double discountPercentage) {
		if (discountPercentage < 0 || discountPercentage > 100) {
			throw new IllegalArgumentException(DISCOUNT_PERCENTAGE_OUT_OF_RANGE);
		}

		return this.quantity * this.calculateDiscountedUnitPrice(discountPercentage);
	}

	/**
	 * Calculate total savings.
	 *
	 * @precondition discountPercetage >= 0 and <= 100
	 * @postcondition none
	 *
	 * @param discountPercentage the discount percentage
	 * @return the total savings based on the discount
	 */
	public double calculateTotalSavings(double discountPercentage) {
		if (discountPercentage < 0 || discountPercentage > 100) {
			throw new IllegalArgumentException(DISCOUNT_PERCENTAGE_OUT_OF_RANGE);
		}

		double total = this.calculateRegularTotal();
		double discountedTotal = this.calculateDiscountedTotal(discountPercentage);
		return total - discountedTotal;

	}

	/**
	 * Gets the unit price.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the unit price
	 */
	public double getUnitPrice() {
		return this.unitPrice;
	}

	/**
	 * Sets the unit price.
	 * 
	 * @precondition unitPrice >= 0
	 * @postcondition getUnitPrice() == unitPrice
	 *
	 * @param unitPrice the new unit price
	 */
	public void setUnitPrice(double unitPrice) {
		if (unitPrice <= 0) {
			throw new IllegalArgumentException(UNIT_PRICE_MUST_BE_GREATER_THAN_0);
		}

		this.unitPrice = unitPrice;
	}

	/**
	 * Gets the quantity.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the quantity
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @precondition quantity >= 0
	 * @postcondition getQuantity() == quantity
	 * 
	 * @param quantity the new quantity
	 */
	public void setQuantity(int quantity) {
		if (quantity <= 0) {
			throw new IllegalArgumentException(QUANTITY_MUST_BE_GREATER_THAN_OR_EQUAL_TO_0);
		}

		this.quantity = quantity;
	}

}
